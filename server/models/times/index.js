const mongoose = require('mongoose')
const Schema = mongoose.Schema

const TimesSchema = new Schema({
  nome: {
    type: String,
    required: true
  },

  formacao: {
    type: String,
    required: true
  },


  dataCriacao: {
    type: Date,
    default: Date.now
  },

  dataAlteracao: {
    type: Date
  }
})

const times = mongoose.model('times', TimesSchema)

module.exports = times