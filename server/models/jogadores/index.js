const mongoose = require('mongoose')
const Schema = mongoose.Schema

const JogadoresSchema = new Schema({
  time: {
    type: Schema.Types.ObjectId,
    ref: 'times',
    required: true
  },

  nome: {
    type: String,
    required: true
  },

  posicao: {
    type: String,
    required: true
  },

  idade: {
    type: Number,
    required: true
  },

  cpf: {
    type: Number,
    required: true
  },

  perna: {
    type: String
  },

  dataCriacao: {
    type: Date,
    default: Date.now
  },

  dataAlteracao: {
    type: Date
  }
})

const jogadores = mongoose.model('jogadores', JogadoresSchema)

module.exports = jogadores