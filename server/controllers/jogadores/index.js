const Jogadores = require('../../models/jogadores')

const get = (req, res) => {
  Jogadores
    .find({ _id: req.params.id })
    .populate('time')
    .lean()
    .then((jogadores) => {
      res.send(jogadores)
    })
    .catch((error) => {
      res.status(500).send({
        mensagem: 'Ocorreu um erro interno.',
        debug: error
      })
    })
}

const find = (req, res) => {
  Jogadores
    .find(req.query)
    .populate('time')
    .lean()
    .then((jogadores) => {
      res.send(jogadores)
    })
    .catch((error) => {
      res.status(500).send({
        mensagem: 'Ocorreu um erro interno.',
        debug: error
      })
    })
}

const create = (req, res) => {
  delete req.body._id
  const jogador = new Jogadores(req.body)

  jogador.save()
    .then((jogador) => {
      res.send(jogador)
  })
    .catch((error) => {
      res.status(500).send({
        mensagem: 'Ocorreu um erro interno.',
        debug: error
      })
    })
}

const update = async (req, res) => {
  const jogador = await Jogadores.findById(req.body._id)

  jogador.set(req.body)

  jogador.set({
    dataAlteracao: new Date()
  })

  jogador.save()
    .then((jogador) => {
      res.send(jogador)
    })
    .catch((error) => {
      res.status(500).send({
        mensagem: 'Ocorreu um erro interno.',
        debug: error
      })
    })
}

const remove = (req, res) => {
  Jogadores.deleteOne({ _id: req.query.id })
    .then((jogador) => {
      res.send(jogador)
    })
    .catch((error) => {
      res.status(500).send({
        mensagem: 'Ocorreu um erro interno.',
        debug: error
      })
    })
}

module.exports = {
  create,
  remove,
  update,
  get,
  find
}