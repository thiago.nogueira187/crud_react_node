const Times = require('../../models/times')

const get = (req, res) => {
  Times
    .find({ _id: req.params.id })
    .lean()
    .then((times) => {
      res.send(times)
    })
    .catch((error) => {
      res.status(500).send({
        mensagem: 'Ocorreu um erro interno.',
        debug: error
      })
    })
}

const find = (req, res) => {
  Times
    .find()
    .lean()
    .then((times) => {
      res.send(times)
    })
    .catch((error) => {
      res.status(500).send({
        mensagem: 'Ocorreu um erro interno.',
        debug: error
      })
    })
}

const create = (req, res) => {
  const time = new Times(req.body)

  time.save()
    .then((time) => {
      res.send(time)
    })
    .catch((error) => {
      res.status(500).send({
        mensagem: 'Ocorreu um erro interno.',
        debug: error
      })
    })
}

const update = async (req, res) => {
  const time = await Times.findById(req.body._id)

  time.set(req.body)

  time.set({
    dataAlteracao: new Date()
  })

  time.save()
    .then((time) => {
      res.send(time)
    })
    .catch((error) => {
      res.status(500).send({
        mensagem: 'Ocorreu um erro interno.',
        debug: error
      })
    })
}

const remove = (req, res) => {
  Times.deleteOne({ _id: req.query.id })
    .then((time) => {
      res.send(time)
    })
    .catch((error) => {
      res.status(500).send({
        mensagem: 'Ocorreu um erro interno.',
        debug: error
      })
    })
}

module.exports = {
  create,
  remove,
  update,
  get,
  find
}