module.exports = {
  api: {
    basePath: '/api'
  },

  http: {
    port: 3000,
    host: 'localhost',
    get fullPath () {
      return `http://${this.host}:${this.port}`
    }
  },

  db: {
    url: 'mongodb+srv://grupo-mobile:UgRKE9McIIiT8YbE@cluster0.nwdmj.gcp.mongodb.net/dbJogadores?retryWrites=true&w=majority'
  }
}