const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const routes = require('./routes')

const cors = require('cors')
const app = express()

app.use(cors())

// parse application/x-www-form-urlencoded
app.use(express.json())
// parse application/json
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
// Include routes
routes(app)

module.exports = app
