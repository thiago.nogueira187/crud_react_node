const express = require('express')
const router = express.Router()
const JogadoresController = require('../../controllers/jogadores')

router.get('/jogadores/:id', JogadoresController.get)
router.get('/jogadores', JogadoresController.find)
router.delete('/jogadores', JogadoresController.remove)
router.put('/jogadores', JogadoresController.update)
router.post('/jogadores', JogadoresController.create)

module.exports = router
