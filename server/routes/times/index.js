const express = require('express')
const router = express.Router()
const TimesController = require('../../controllers/times')

router.get('/times/:id', TimesController.get)
router.delete('/times', TimesController.remove)
router.get('/times', TimesController.find)
router.put('/times', TimesController.update)
router.post('/times', TimesController.create)

module.exports = router
