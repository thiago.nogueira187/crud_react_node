import React from 'react';
import './ListaTimes.css';
import FormTimes from '../FormTimes/FormTimes'
import config from '../../config'

class ListaTimes extends React.Component {
  constructor () {
    super()

    this.handleDialog = this.handleDialog.bind(this);
    this.handleRemove = this.handleRemove.bind(this);
    this.getTimes = this.getTimes.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.refreshTimes = this.refreshTimes.bind(this);

    this.state = {
      showModal: false,
      times: [],
      time: {}
    }
  }
  
  componentDidMount () {
    this.getTimes()
  }

  closeModal () {
    this.setState({ showModal: false })
  }

  getTimes () {
    return fetch(`${config.url}/api/times`)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            times: result
          });
        },
        (error) => {
          console.log(error)
        }
      )
  }

  refreshTimes (time) {
    this.getTimes()
      .then(() => {
        this.props.setTime(time)
      })
  }

  handleDialog () {
    this.setState({ showModal: true, time: {} })
    this.props.setTime({})
  }

  handleEdit (time) {
    this.setState({ showModal: true, time })

    this.props.setTime(time)
  }

  handleRemove (_id) {
    fetch(`${config.url}/api/times?id=${_id}`, {
      method: 'DELETE',
      body: JSON.stringify(this.state),
      headers:{
        'Content-Type': 'application/json'
       }
    })
    .then(res => {
      res.json()

      return this.getTimes()
    })
    .then(response => console.log('Success:', JSON.stringify(response)))
    .catch(error => console.error('Error:', error));
  }

  render () {
    return (
      <div className="ListaTimes">
        <ul>
          <li>
            <div className="row">
              <div className="col-8">
                <h1>Times</h1>
              </div>

              <div className="col-4 col-button" onClick={this.handleDialog}>
                <button className="btn btn-sm btn-primary" type="button">
                  Adicionar time
                </button>
              </div>
            </div>
          </li>

          <FormTimes
            showModal={this.state.showModal}
            time={this.state.time}
            getTimes={this.refreshTimes}
            closeModal={this.closeModal}></FormTimes>

          {this.state.times.map(time => {
            return (
              <li key={time._id} className="p-2">
                <div className="row">
                  <div className="col">

                    <div className={`card-time ${time._id === this.state.time._id ? 'active': ''}`}>
                      <div className="row">
                        <div className="col-8">
                          <p><b>Nome:</b> {time.nome}</p>
                          <p><b>Formação:</b> {time.formacao}</p>
                        </div>

                        <div className="col-4 text-right">
                          <button className="btn btn-info btn-sm mr-1" type="button" onClick={event => this.handleEdit(time)}>Editar</button>
                          <button className="btn btn-danger btn-sm" type="button" onClick={event => this.handleRemove(time._id)}>Excluir</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
            )
          })}
        </ul>
      </div>
    );
  }
}

export default ListaTimes;
