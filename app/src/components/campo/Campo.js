import React from 'react';
import './Campo.css';
import FormJogadores from '../FormJogadores/FormJogadores'
import config from '../../config'

class Campo extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      showModal: false,
      posicao: '',
      jogadores: [],
      time: {}
    }

    this.handleForm = this.handleForm.bind(this);
    this.renderizarCampo = this.renderizarCampo.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.getJogadores = this.getJogadores.bind(this);
    this.getJogadorByPosicao = this.getJogadorByPosicao.bind(this);
    this.getClassActiveJogador = this.getClassActiveJogador.bind(this);
    this.handleRemove = this.handleRemove.bind(this);
  }

  componentWillReceiveProps(props) {
    const { time = {} } = props

    if (time._id !== this.state.time._id) {
      this.getJogadores(time._id)
    }

    this.setState({ time })
  }

  handleForm(posicao) {
    this.setState({ showModal: true, posicao, jogador: this.getJogadorByPosicao(posicao) });
  }

  closeModal() {
    this.setState({ showModal: false });
  }

  getJogadores(timeId) {
    fetch(`${config.url}/api/jogadores?time=${timeId}`)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            jogadores: result
          });
        },
        (error) => {
          console.log(error)
        }
      )
  }

  getJogadorByPosicao(posicao) {
    return this.state.jogadores.find(jogador => jogador.posicao === posicao)
  }

  getClassActiveJogador(posicao) {
    return this.getJogadorByPosicao(posicao) ? 'active' : ''
  }

  handleRemove(posicao) {
    const { _id } = this.getJogadorByPosicao(posicao)

    fetch(`${config.url}/api/jogadores?id=${_id}`, {
      method: 'DELETE',
      body: JSON.stringify(this.state),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(res => {
        res.json()

        const index = this.state.jogadores.findIndex(jogador => jogador._id === _id)

        if (index >= 0) {
          let jogadores = [...this.state.jogadores]

          jogadores.splice(index, 1)

          this.setState({ jogadores })
        }
      })
      .catch(error => console.error('Error:', error));
  }

  render() {
    if (this.state.time._id) {
      return (
        <div className="Campo">

          <div>
            <span className="text-center d-block"><b>Time:</b> {this.state.time.nome}</span>
            <span className="text-center d-block"><b>Formação:</b> {this.state.time.formacao}</span>
          </div>

          {this.renderizarCampo(this.state.time.formacao)}

          <FormJogadores
            jogador={this.state.jogador}
            showModal={this.state.showModal}
            time={this.props.time}
            posicao={this.state.posicao}
            getJogadores={this.getJogadores}
            closeModal={this.closeModal}>
          </FormJogadores>
        </div>
      );
    }

    return (
      <div>
        <p className="text-center">Selecione um time</p>
      </div>
    )
  }

  renderizarCampo(formacao) {
    let classCampo = 'field__compo1'

    if (formacao === '3:5:2') {
      classCampo = 'field__compo2'
    }

    if (formacao === '4:5:1') {
      classCampo = 'field__compo3'
    }

    if (formacao === '4:3:3') {
      classCampo = 'field__compo4'
    }

    return (
      <ul className={`field ${classCampo}`}>
        <li className={`field__player field__player--goal ${this.getClassActiveJogador('goal')}`}>
          <span className="badge badge-danger" onClick={() => this.handleRemove('goal')}>×</span>
          <div className="player-capture" onClick={() => this.handleForm('goal')}></div>
        </li>

        <li className={`field__player field__player--defender-1 ${this.getClassActiveJogador('defender-1')}`}>
          <span className="badge badge-danger" onClick={() => this.handleRemove('defender-1')}>×</span>
          <div className="player-capture" onClick={() => this.handleForm('defender-1')}></div>
        </li>

        <li className={`field__player field__player--defender-2 ${this.getClassActiveJogador('defender-2')}`}>
          <span className="badge badge-danger" onClick={() => this.handleRemove('defender-2')}>×</span>
          <div className="player-capture" onClick={() => this.handleForm('defender-2')}></div>
        </li>

        <li className={`field__player field__player--defender-3 ${this.getClassActiveJogador('defender-3')}`}>
          <span className="badge badge-danger" onClick={() => this.handleRemove('defender-3')}>×</span>
          <div className="player-capture" onClick={() => this.handleForm('defender-3')}></div>
        </li>

        <li className={`field__player field__player--defender-4 ${this.getClassActiveJogador('defender-4')}`}>
          <span className="badge badge-danger" onClick={() => this.handleRemove('defender-4')}>×</span>
          <div className="player-capture" onClick={() => this.handleForm('defender-4')}></div>
        </li>

        <li className={`field__player field__player--middle-1 ${this.getClassActiveJogador('middle-1')}`}>
          <span className="badge badge-danger" onClick={() => this.handleRemove('middle-1')}>×</span>
          <div className="player-capture" onClick={() => this.handleForm('middle-1')}></div>
        </li>

        <li className={`field__player field__player--middle-2 ${this.getClassActiveJogador('middle-2')}`}>
          <span className="badge badge-danger" onClick={() => this.handleRemove('middle-2')}>×</span>
          <div className="player-capture" onClick={() => this.handleForm('middle-2')}></div>
        </li>

        <li className={`field__player field__player--middle-3 ${this.getClassActiveJogador('middle-3')}`}>
          <span className="badge badge-danger" onClick={() => this.handleRemove('middle-3')}>×</span>
          <div className="player-capture" onClick={() => this.handleForm('middle-3')}></div>
        </li>

        <li className={`field__player field__player--middle-4 ${this.getClassActiveJogador('middle-4')}`}>
          <span className="badge badge-danger" onClick={() => this.handleRemove('middle-4')}>×</span>
          <div className="player-capture" onClick={() => this.handleForm('middle-4')}></div>
        </li>

        <li className={`field__player field__player--attack-1 ${this.getClassActiveJogador('attack-1')}`}>
          <span className="badge badge-danger" onClick={() => this.handleRemove('attack-1')}>×</span>
          <div className="player-capture" onClick={() => this.handleForm('attack-1')}></div>
        </li>

        <li className={`field__player field__player--attack-2 ${this.getClassActiveJogador('attack-2')}`}>
          <span className="badge badge-danger" onClick={() => this.handleRemove('attack-2')}>×</span>
          <div className="player-capture" onClick={() => this.handleForm('attack-2')}></div>
        </li>
      </ul>
    )
  }
}


export default Campo;
