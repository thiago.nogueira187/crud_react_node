import React from 'react';
import './FormTimes.css'
import config from '../../config'

class FormTimes extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      formacao: '',
      nome: '',
      _id: ''
    }

    this.handleChangeNome = this.handleChangeNome.bind(this);
    this.handleChangeFormacao = this.handleChangeFormacao.bind(this);
    this.handleSave = this.handleSave.bind(this);
  }

  componentWillReceiveProps(props) {
    const { nome, formacao, _id } = props.time

    this.setState({ nome, formacao, _id })
  }

  handleChangeNome(event) {
    this.setState({ nome: event.target.value });
  }

  handleChangeFormacao(event) {
    this.setState({ formacao: event.target.value });
  }

  handleSave() {
    let method = 'POST'

    if (this.state._id) {
      method = 'PUT'
    }

    fetch(`${config.url}/api/times`, {
      method: method,
      body: JSON.stringify(this.state),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(res => res.json())
      .then(time => {
        return this.props.getTimes(time)
      })
      .then(() => {
        this.props.closeModal()
      })
      .catch(error => console.error('Error:', error));
  }

  render() {
    if (this.props.showModal) {
      return (
        <div className="modal" role="dialog">
          <div className="modal-dialog" role="document">
            <div className="modal-content">

              <div className="modal-header">
                <h2>{this.state._id ? 'Editar': 'Cadastrar'} Time</h2>
                <button type="button" className="btn-close" onClick={this.props.closeModal}>×</button>
              </div>

              <div className="modal-body">
                <form className="mt-1 mb-2">
                  <div className="form-group">
                    <label>Nome</label>
                    <input placeholder="Informe o nome" value={this.state.nome} className="w-100 form-control" onChange={this.handleChangeNome}></input>
                  </div>

                  <div className="form-group">
                    <label>Formação</label>
                    <select value={this.state.formacao} className="w-100 form-control" onChange={this.handleChangeFormacao}>
                      <option value="">Selecione formação</option>
                      <option value="4:4:2">4:4:2</option>
                      <option value="3:5:2">3:5:2</option>
                      <option value="4:5:1">4:5:1</option>
                      <option value="4:3:3">4:3:3</option>
                    </select>
                  </div>

                  <div className="col-save">
                    <button type="button" className="btn btn-success" onClick={this.handleSave}>Salvar</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      );
    }

    return ''
  }
}

export default FormTimes;
