import React from 'react';
import './FormJogadores.css'
import config from '../../config'

class FormJogadores extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            _id: '',
            idade: '',
            cpf: '',
            perna: '',
            formacao: '',
            time: '',
            posicaoLabel: '',
            nome: '',
            posicao: ''
        }

        this.handleChangeNome = this.handleChangeNome.bind(this);
        this.handleChangeIdade = this.handleChangeIdade.bind(this);
        this.handleChangeCPF = this.handleChangeCPF.bind(this);
        this.handleChangePerna = this.handleChangePerna.bind(this);
        this.handleSave = this.handleSave.bind(this);
    }

    componentWillReceiveProps(props) {
        const defaultJogador = {
            _id: '',
            idade: '',
            cpf: '',
            perna: '',
            formacao: '',
            nome: ''
        }

        const { time = {}, posicao, jogador = defaultJogador } = props
        this.setState({ posicaoLabel: this.getNomePosicao(posicao), posicao, time: time._id, ...jogador })
    }

    getNomePosicao(posicao = '') {
        if (posicao.includes('middle')) return 'Meio de campo'
        if (posicao.includes('goal')) return 'Goleiro'
        if (posicao.includes('attack')) return 'Atacante'

        return 'Defensor'
    }

    handleChangeNome(event) {
        this.setState({ nome: event.target.value });
    }

    handleChangeIdade(event) {
        this.setState({ idade: event.target.value });
    }

    handleChangeCPF(event) {
        this.setState({ cpf: event.target.value });
    }

    handleChangePerna(event) {
        this.setState({ perna: event.target.value });
    }

    handleSave() {
        let method = 'POST'

        if (this.state._id) {
          method = 'PUT'
        }

        fetch(`${config.url}/api/jogadores`, {
            method,
            body: JSON.stringify(this.state),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(res => res.json())
            .then(() => this.props.getJogadores(this.props.time._id))
            .then(() => this.props.closeModal())
            .catch(error => console.error('Error:', error));
    }

    render() {
        if (this.props.showModal) {
            return (
                <div className="modal" role="dialog">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">

                            <div className="modal-header">
                                <h2>{this.state._id ? 'Editar': 'Cadastrar'} Jogador</h2>
                                <button type="button" className="btn-close" onClick={this.props.closeModal}>×</button>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="row mb-2">
                                        <div className="col-7">
                                            <span>Time: {this.props.time.nome}</span>
                                        </div>

                                        <div className="col">
                                            <span className="text-right">Posição: {this.state.posicaoLabel}</span>
                                        </div>
                                    </div>

                                    <div className="mb-1">
                                        <label>Nome</label>
                                        <input className="form-control" placeholder="Informe o nome" value={this.state.nome} onChange={this.handleChangeNome}></input>
                                    </div>

                                    <div className="mb-1">
                                        <label>Idade</label>
                                        <input className="form-control" type="number" placeholder="Informe o nome" value={this.state.idade} onChange={this.handleChangeIdade}></input>
                                    </div>

                                    <div className="mb-1">
                                        <label>CPF</label>
                                        <input className="form-control" type="number" placeholder="Informe o nome" value={this.state.cpf} onChange={this.handleChangeCPF}></input>
                                    </div>

                                    <div className="mb-1">
                                        <label>Perna</label>
                                        <select className="form-control" value={this.state.perna} onChange={this.handleChangePerna}>
                                            <option value="">Selecione Perna</option>
                                            <option value="destro">Destro</option>
                                            <option value="canhoto">Canhoto</option>
                                            <option value="Ambidestro">Ambidestro</option>
                                        </select>
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-success btn-sm" onClick={this.handleSave}>Salvar</button>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }

        return ''
    }
}

export default FormJogadores;
