export default {
    host: 'localhost',
    port: 3000,
    get url () {
        return `http://${this.host}:${this.port}`
    }
}