import React from 'react';
import './App.css';
import Campo from './components/campo/Campo'
import ListaTimes from './components/ListaTimes/ListaTimes'

class App extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      time: {}
    }

    this.setTime = this.setTime.bind(this);
  }


  render () {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-5">
            <ListaTimes setTime={this.setTime}></ListaTimes>
          </div>
  
          <div className="col-7">
            <Campo time={this.state.time}></Campo>
          </div>
        </div>
      </div>
    );
  }

  setTime (timeSelecionado) {
    this.setState({time: timeSelecionado})
  }
}


export default App;
