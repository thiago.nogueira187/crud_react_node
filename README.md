## Descrição do projeto

CRUD para a disciplina de dispositivos moveis, com o tema de time de futebol, tendo cadastro, atualização e remoção do time e dos jogadores dos times de uma maneira intuitiva

## Tecnologios
 - NodeJS
 - MongoDB
 - React


## Video do projeto

![Video projeto](doc/video_projeto.mp4)
